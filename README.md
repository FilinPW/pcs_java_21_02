# Домашние задания

---
[Тема 1. Введение в специальность. Основы информатики - архитектура компьютера, системы счисления](Projects/01. Intro/README.md)
---

---
[Тема 3. Языки программирования, cтруктуры управления, типы данных, построение алгоритмов](Projects/03. Algorithms/README.md)
---

---
[Тема 4. Инфраструктура Java, первая программа, массивы](Projects/04. java and little bit array/README.md)
---
