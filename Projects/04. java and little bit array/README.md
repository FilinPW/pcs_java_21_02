# Домашнее задание

## Homework - 04

```
PS E:\Innopolis\GitLab\pcs_java_21_02> javac -version
javac 17

PS E:\Innopolis\GitLab\pcs_java_21_02> java version
Error: Could not find or load main class version
Caused by: java.lang.ClassNotFoundException: version

PS E:\Innopolis\GitLab\pcs_java_21_02> java -version
java version "17" 2021-09-14 LTS
Java(TM) SE Runtime Environment (build 17+35-LTS-2724)
Java HotSpot(TM) 64-Bit Server VM (build 17+35-LTS-2724, mixed mode, sharing)
```

---
[Дополнительное задание](https://gitlab.com/FilinPW/pcs_java_21_02/-/blob/main/Projects/04.%20java%20and%20little%20bit%20array/src/Intro.java)
---