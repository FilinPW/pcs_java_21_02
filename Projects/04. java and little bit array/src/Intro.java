import java.util.Scanner;

public class Intro {
    public static void main(String arrgs[]) {
        int lmCount = 1;
        int a, b, c;
        Scanner in = new Scanner(System.in);
        System.out.println("Поиск локальных минимумов.");
        System.out.println("Задайте последовательность чисел, где -1 последнее число.");

        b = in.nextInt();
        if (b == -1) {
            System.out.println("Количество локальных минимумов: " + lmCount);
            return;
        }

        a = in.nextInt();
        if (a == -1) {
            System.out.println("Количество локальных минимумов: " + lmCount);
            return;
        }
        if (b < a) {
            lmCount++;
        }

        c = b;
        b = a;
        a = in.nextInt();

        while (a != -1) {
            if (a > b && b < c) {
                lmCount++;
            }
            c = b;
            b = a;
            a = in.nextInt();
        }

        System.out.println("Количество локальных минимумов: " + lmCount);
    }
}
